# TheCodeine EditorBundle

## Standalone usage:
To use CKEditor include jquery and CKE (`bundles/thecodeineeditor/js/vendor/ckeditor/ckeditor.js`). You also have to define base path for CKE as global JS variable:

    var CKEDITOR_BASEPATH = 'bundles/thecodeineeditor/js/vendor/ckeditor/';